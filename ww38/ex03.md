
PART 1

link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
inet 127.0.0.1/8 scope host lo
valid_lft forever preferred_lft forever
inet6 ::1/128 scope host
valid_lft forever preferred_lft forever

IP addresses assinged to different network connections

PART2
default via 192.168.5.2 dev eth0 proto dhcp metric 100
192.168.5.0/24 dev eth0 proto kernel scope link src 192.168.5.128 metric 100

The route that the internet packages takes.